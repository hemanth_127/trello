import './App.css'
import Home from './Components/Home'
import Lists from './TrelloComponent/Lists/Lists.jsx'
import { Routes, Route } from 'react-router-dom'
import Header from './Components/Header.jsx'
import ErrorProvider from './Components/ErrorContext.jsx'
import NoMatch from './Components/NoMatch.jsx'
function App () {
  return (
    <ErrorProvider>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/lists/:id' element={<Lists />} />
        <Route path='*' element={<NoMatch />} />
      </Routes>
    </ErrorProvider>
  )
}

export default App


