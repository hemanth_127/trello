import React, { useContext } from 'react'
import { createCheckListById } from '../../Components/Api.js'
import Form from '../Lists/Form.jsx'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
const AddNewChecklist = ({
  setShowAddChecklist,
  checkListData,
  setCheckListData,
  selectedCardId
}) => {
  const { setError } = useContext(ErrorContext)

  const handleCancelCheckList = () => {
    setShowAddChecklist(false)
  }

  const handleCreateCheckList = async name => {
    try {
      const res = await createCheckListById(selectedCardId, name)
      setCheckListData([...checkListData, res.data])
      setShowAddChecklist(false)
    } catch (err) {
      setError(err.message)
    }
  }

  return (
    <>
      <div className='modal-content'>
        <Form
          handleAddTo={handleCreateCheckList}
          handleCancel={handleCancelCheckList}
          className={'checkList-add'}
        />
      </div>
    </>
  )
}

export default AddNewChecklist
