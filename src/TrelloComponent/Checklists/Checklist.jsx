import React, { useEffect, useState, useContext } from 'react'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Dialog from '@mui/material/Dialog'
import CloseIcon from '@mui/icons-material/Close'
import DomainVerificationIcon from '@mui/icons-material/DomainVerification'
import CreditCardOutlinedIcon from '@mui/icons-material/CreditCardOutlined'
import CheckItems from '../ChecItems/CheckItems.jsx'
import AddNewChecklist from '../Checklists/AddNewChecklist.jsx'
import { getCheckList, deleteCheckListById } from '../../Components/Api.js'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
import Spinner from '../../Components/Spinner'

const StyledDialog = styled(Dialog)(() => ({
  '& .MuiPaper-rounded ': {
    width: '-webkit-fill-available',
    height: '-webkit-fill-available',
    backgroundColor: '#323940'
  }
}))

export default function Checklist ({
  setShowChecklist,
  showChecklist,
  selectedCard
}) {
  const [checkListData, setCheckListData] = useState([])
  const [showAddChecklist, setShowAddChecklist] = useState(false)
  const [loading, setLoading] = useState(false)
  const { setError } = useContext(ErrorContext)

  const fetchChecklist = async () => {
    setLoading(true)
    try {
      const res = await getCheckList(selectedCard[0])
      const data = await res.data
      setCheckListData(data)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchChecklist()
  }, [selectedCard[0]])

  const handleDeleteCheckList = async (cardId, checkListId) => {
    try {
      await deleteCheckListById(cardId, checkListId)
      setCheckListData(checkListData.filter(Checklist => Checklist.id !== checkListId))
    } catch (err) {
      setError(err.message)
    }
  }

  const handleCloseCheckList = () => {
    setShowChecklist(false)
  }

  return (
    <StyledDialog
      onClose={handleCloseCheckList}
      aria-labelledby='customized-dialog-title'
      open={showChecklist}
      className='checklistDialog'
    >
      <div className='checklist-container'>
        <div className='checklistContent'>
          <div className='checklist-header'>
            <div className='checklist'>
              <CreditCardOutlinedIcon size='small' />
              <h3 className='spacerleft'>{selectedCard[1]}</h3>
            </div>
            <div>
              <Button size='small' onClick={handleCloseCheckList}>
                <CloseIcon sx={{ color: '#B6C2CF' }} />
              </Button>
            </div>
          </div>

          <div className='divider'>
            <div className='checklist-body'>
              {loading ? (
                <Spinner />
              ) : (
                <>
                  {checkListData.map(ele => {
                    return (
                      <div className='Checklistdata' key={ele.id}>
                        <div
                          className='check-items'
                          id='items'
                          style={{ marginBottom: '.5rem' }}
                        >
                          <div className='checklist'>
                            <DomainVerificationIcon size='small' />
                            <h4 className='spacerleft'>{ele.name}</h4>
                          </div>

                          <div>
                            <Button
                              size='small'
                              variant='contained'
                              sx={{
                                color: '#B6C2CF',
                                backgroundColor: '#3B444C'
                              }}
                              onClick={() => {
                                handleDeleteCheckList(selectedCard[0], ele.id)
                              }}
                            >
                              delete
                            </Button>
                          </div>
                        </div>

                        <CheckItems
                          selectedCardId={selectedCard[0]}
                          checkListId={ele.id}
                        />
                      </div>
                    )
                  })}
                </>
              )}
            </div>

            {showAddChecklist ? (
              <AddNewChecklist
                setShowAddChecklist={setShowAddChecklist}
                checkListData={checkListData}
                setCheckListData={setCheckListData}
                selectedCardId={selectedCard[0]}
              />
            ) : (
              <div
                className='check-item-addition'
                onClick={() => {
                  setShowAddChecklist(true)
                }}
              >
                <p style={{ marginBottom: '.5rem' }}>Add to card</p>
                <Button
                  variant='contained'
                  sx={{ color: '#B6C2CF', backgroundColor: '#3B444C' }}
                >
                  <DomainVerificationIcon size='small' />
                  <p className='spacerleft'> Checklist </p>
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </StyledDialog>
  )
}
