import React, { useContext, useEffect, useState } from 'react'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { CardActionArea } from '@mui/material'
import { NavLink } from 'react-router-dom'
import DialogBox from './Dialog.jsx'
import { createBoardByName, getAllBoards } from '../../Components/Api.js'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
import Spinner from '../../Components/Spinner.jsx'

const Board = () => {
  const { setError } = useContext(ErrorContext)
  const [boardsData, setBoardsData] = useState([])
  const [openDialog, setOpenDialog] = useState(false)
  const [readBoardName, setReadBoardName] = useState('')
  const [loading, setLoading] = useState(false)

  const fetchBoards = async () => {
    setLoading(true)
    try {
      const response = await getAllBoards()
      setBoardsData(response.data)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchBoards()
  }, [])

  const createBoard = async name => {
    try {
      const response = await createBoardByName(name)
      setBoardsData([...boardsData, response.data])
      setReadBoardName('')
      handleCloseDialog()
    } catch (err) {
      setError(err.message)
    }
  }

  const handleOpenDialog = () => {
    setOpenDialog(true)
  }

  const handleCloseDialog = () => {
    setOpenDialog(false)
  }

  return (
    <>
      <div style={{ display: 'flex' }}>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {loading ? (
            <div style={{ marginLeft: '40rem', marginTop: '15rem' }}>
              <Spinner />
            </div>
          ) : (
            <>
              {boardsData.map(item => (
                <NavLink
                  to={`/lists/${item.id}`}
                  key={item.id}
                  style={{ textDecoration: 'none' }}
                >
                  <Card
                    variant='primary'
                    key={`${item.id}`}
                    sx={{
                      width: '15vw',
                      m: 4,
                      p: 3,
                      height: '8rem',
                      cursor: 'pointer',
                      borderRadius: '1rem'
                    }}
                  >
                    <CardActionArea>
                      <Typography gutterBottom variant='h6' component='div'>
                        {item.name}
                      </Typography>
                    </CardActionArea>
                  </Card>
                </NavLink>
              ))}

              <div className='add-board'>
                <div onClick={handleOpenDialog}>
                  <Typography gutterBottom variant='h6' component='div'>
                    Create New Board
                  </Typography>
                </div>

                <DialogBox
                  open={openDialog}
                  handleBoardClose={handleCloseDialog}
                  createBoard={createBoard}
                  readBoardName={readBoardName}
                  setReadBoardName={setReadBoardName}
                />
              </div>
            </>
          )}
        </div>
      </div>
    </>
  )
}

export default Board
