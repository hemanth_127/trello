import React from 'react'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'

const DialogBox = ({
  open,
  handleBoardClose,
  createBoard,
  readBoardName,
  setReadBoardName
}) => {
  const handleSubmit = event => {
    event.preventDefault()
    createBoard(readBoardName)
  }

  return (
    <Dialog
      open={open}
      onClose={handleBoardClose}
      PaperProps={{ component: 'form', onSubmit: handleSubmit }}
    >
      <DialogTitle>New Board</DialogTitle>

      <DialogContent>
        <DialogContentText>Enter title of the new board</DialogContentText>
        <TextField
          autoFocus
          required
          margin='dense'
          id='name'
          name='name'
          label='Board title '
          variant='standard'
          type='text'
          fullWidth
          value={readBoardName}
          onChange={e => setReadBoardName(e.target.value)}
        />
      </DialogContent>

      <DialogActions>
        <Button
          onClick={() => {
            handleBoardClose()
          }}
        >
          Cancel
        </Button>
        <Button type='submit'>Create</Button>
      </DialogActions>
    </Dialog>
  )
}

export default DialogBox
