import React, { useEffect, useContext, useState } from 'react'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import CircularProgress from '@mui/material/CircularProgress'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
import Checklist from '../Checklists/Checklist.jsx'
import Form from '../Lists/Form.jsx'
import Spinner from '../../Components/Spinner'

import {
  getAllCards,
  deleteCardById,
  createCard
} from '../../Components/Api.js'

const Cards = ({ listId }) => {
  const { setError } = useContext(ErrorContext)
  const [cardsData, setCardsData] = useState([])
  const [showCardForm, setShowCardForm] = useState(false)
  const [showChecklist, setShowChecklist] = useState(false)
  const [selectedCard, setSelectedCard] = useState([])
  const [loading, setLoading] = useState(false)

  const fetchCardsData = async () => {
    setLoading(true)
    try {
      let res = await getAllCards(listId)
      const data = await res.data
      setCardsData(data)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchCardsData()
  }, [])

  const handleCreateCard = async name => {
    try {
      const res = await createCard(listId, name)
      const data = await res.data
      setCardsData([...cardsData, data])
      setShowCardForm(false)
    } catch (err) {
      setError(err.message)
    }
  }

  const handleDeleteCard = async cardId => {
    try {
      deleteCardById(cardId)
      setCardsData(cardsData.filter(card => card.id !== cardId))
    } catch (err) {
      setError(err.message)
    }
  }

  const handleCancelCard = () => {
    setShowCardForm(false)
  }

  return (
    <ol className='innerlist'>
      {showChecklist && (
        <Checklist
          setShowChecklist={setShowChecklist}
          showChecklist={showChecklist}
          selectedCard={selectedCard}
        />
      )}
      
      {loading ? (
        <Spinner  />
      ) : (
        <>
          {cardsData.map(card => {
            return (
              <li
                key={card.id}
                className='carditem'
                onClick={() => {
                  setSelectedCard([card.id, card.name])
                  setShowChecklist(true)
                }}
              >
                {card.name}
                <DeleteIcon
                  fontSize='medium'
                  onClick={e => {
                    e.stopPropagation()
                    handleDeleteCard(card.id)
                  }}
                  sx={{ cursor: 'pointer', padding: '3px', ml: 1 }}
                />
              </li>
            )
          })}
        </>
      )}

      {showCardForm ? (
        <div>
          <Form
            handleAddTo={handleCreateCard}
            handleCancel={handleCancelCard}
            className={''}
          />
        </div>
      ) : (
        <li
          className=''
          id='add-cardItem'
          onClick={() => {
            setShowCardForm(true)
          }}
        >
          <AddIcon />
          <h4>Add New card</h4>
        </li>
      )}
    </ol>
  )
}

export default Cards
