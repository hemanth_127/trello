import React, { useState, useEffect, useContext } from 'react'
import {
  getCheckItems,
  deleteCheckItemById,
  strikeCheckItems
} from '../../Components/Api.js'
import Button from '@mui/material/Button'
import AddNewChecItem from '../ChecItems/AddNewChecItem.jsx'
import DeleteIcon from '@mui/icons-material/Delete'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
import ProgressBar from './ProgressBar.jsx'
import Spinner from '../../Components/Spinner.jsx'

const CheckItems = ({ checkListId, selectedCardId }) => {
  const { setError } = useContext(ErrorContext)
  const [checkItemData, setCheckItemData] = useState([])
  const [showCheckItem, setShowCheckItem] = useState(false)
  const [loading, setLoading] = useState(false)

  const fetchCheckItem = async checkListId => {
    setLoading(true)
    try {
      const res = await getCheckItems(checkListId)
      const data = await res.data
      setCheckItemData(data)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchCheckItem(checkListId)
  }, [checkListId])

  const handleDeleteCheckItem = async CheckItemId => {
    try {
      await deleteCheckItemById(checkListId, CheckItemId)
      setCheckItemData(checkItemData =>
        checkItemData.filter(ele => ele.id != CheckItemId)
      )
    } catch (err) {
      setError(err.message)
    }
  }

  const toggleCheckedStateItem = async (checkItemId, currentState) => {
    try {
      const newState = currentState === 'complete' ? 'incomplete' : 'complete'

      await strikeCheckItems(selectedCardId, checkListId, checkItemId, newState)

      const updatedCheckItems = checkItemData.map(item => {
        if (item.id === checkItemId) {
          return {
            ...item,
            state: newState
          }
        }
        return item
      })

      setCheckItemData(updatedCheckItems)
    } catch (error) {
      setError(error.message)
    }
  }

  const totalChecked = !checkItemData ? 0 : checkItemData.length
  let completedCount = 0
  checkItemData.map(ele => {
    if (ele.state === 'complete') {
      completedCount += 1
    }
  })

  const progressPercentage =
    totalChecked === 0 ? 0 : (completedCount / checkItemData.length) * 100

  return (
    <div className='checklist-items'>
      <ProgressBar progress={progressPercentage} />
      {loading ? (
        <Spinner />
      ) : (
        <>
          {checkItemData.map(item => (
            <div className='checklist-tems-group' key={item.id}>
              <div className='checklist-checkbox'>
                <input
                  type='checkbox'
                  checked={item.state === 'complete'}
                  onChange={() => toggleCheckedStateItem(item.id, item.state)}
                />

                <p
                  className='spacerleft'
                  style={{
                    textDecoration:
                      item.state === 'complete' ? 'line-through' : 'none'
                  }}
                >
                  {item.name}
                </p>
              </div>

              <div className='checklist-text'>
                <DeleteIcon
                  sx={{ height: '1.2rem', width: '1.5rem', cursor: 'pointer' }}
                  size='small'
                  onClick={() => {
                    handleDeleteCheckItem(item.id)
                  }}
                />
              </div>
            </div>
          ))}
        </>
      )}

      {showCheckItem ? (
        <div style={{ margin: '.8rem' }}>
          <AddNewChecItem
            setShowCheckItem={setShowCheckItem}
            checkListId={checkListId}
            setCheckItemData={setCheckItemData}
            checkItemData={checkItemData}
          />
        </div>
      ) : (
        <div className='add-cardItem'>
          <Button
            variant='contained'
            sx={{
              color: '#B6C2CF',
              backgroundColor: '#3B444C',
              marginTop: '1rem',
              marginLeft: '1.4rem'
            }}
            onClick={() => {
              setShowCheckItem(true)
            }}
          >
            Add an Item
          </Button>
        </div>
      )}
    </div>
  )
}

export default CheckItems
