import React, { useContext } from 'react'
import Form from '../Lists/Form.jsx'
import { createCheckItems } from '../../Components/Api.js'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
const AddNewChecItem = ({
  setShowCheckItem,
  checkListId,
  checkItemData,
  setCheckItemData
}) => {
  const { setError } = useContext(ErrorContext)

  const handleCreateCheckItem = async name => {
    try {
      const res = await createCheckItems(checkListId, name)
      const data = await res.data
      setCheckItemData([...checkItemData, data])
      setShowCheckItem(false)
    } catch (err) {
      setError(err.message)
    }
  }

  const handleCancelCheckItem = () => {
    setShowCheckItem(false)
  }

  return (
    <Form
      handleAddTo={handleCreateCheckItem}
      handleCancel={handleCancelCheckItem}
      className={''}
    />
  )
}

export default AddNewChecItem
