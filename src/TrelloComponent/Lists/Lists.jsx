import React from 'react'
import { useParams } from 'react-router-dom'
import { useState } from 'react'
import {
  getAllLists,
  createList,
  deleteListById
} from '../../Components/Api.js'
import { useEffect, useContext } from 'react'
import Cards from '../Cards/Cards.jsx'
import DeleteIcon from '@mui/icons-material/Delete'
import AddIcon from '@mui/icons-material/Add'
import ListForm from './Form.jsx'
import { ErrorContext } from '../../Components/ErrorContext.jsx'
import Spinner from '../../Components/Spinner.jsx'

const Lists = () => {
  const { setError } = useContext(ErrorContext)
  const [loading, setLoading] = useState(false)
  const [listsData, setListsData] = useState([])
  const [showListForm, setShowListForm] = useState(false)
  const id = useParams().id

  const fetchListData = async () => {
    setLoading(true)
    try {
      const res = await getAllLists(id)
      const data = await res.data
      setListsData(data)
    } catch (err) {
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchListData()
  }, [])

  const handleDeleteList = async listId => {
    try {
      await deleteListById(listId)
      setListsData(listsData.filter(ele => ele.id !== listId))
    } catch (error) {
      setError(error.message)
    }
  }

  const handleCreateList = async name => {
    try {
      const res = await createList(id, name)
      setListsData([...listsData, res.data])
      setShowListForm(false)
    } catch (err) {
      setError(err.message)
    }
  }

  const handleCancelList = () => {
    setShowListForm(false)
  }

  return (
    <>
      <div>
        <div className='list' style={{ marginTop: '4rem' }}>
          <div className='list-items'>
            {loading ? (
              <div style={{marginLeft:"40rem",marginTop:'15rem'}}>
                <Spinner />
              </div>
            ) : (
              <>
                {listsData &&
                  listsData.map(list => {
                    return (
                      <div key={list.id}>
                        <div className='list-item' key={list.id}>
                          <h3>{list.name}</h3>
                          <DeleteIcon
                            className='closeIcon'
                            onClick={() => handleDeleteList(list.id)}
                          />
                        </div>

                        <Cards listId={list.id} />
                      </div>
                    )
                  })}

                {showListForm ? (
                  <div className='cardadder'>
                    <ListForm
                      handleAddTo={handleCreateList}
                      handleCancel={handleCancelList}
                      className={'listadder'}
                    />
                  </div>
                ) : (
                  <div style={{ width: '20vw' }}>
                    <div
                      className='list-item'
                      id='list-addItem'
                      onClick={() => {
                        setShowListForm(true)
                      }}
                    >
                      <AddIcon className='closeIcon' />
                      <h4>Add another list</h4>
                    </div>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
export default Lists
