import React from 'react'
import Textarea from '@mui/joy/Textarea'
import Button from '@mui/joy/Button'
import CloseIcon from '@mui/icons-material/Close'
import { useState } from 'react'

const ListForm = ({ handleAddTo, handleCancel, className = 'listaddr' }) => {
  const [inputValue, setInputValue] = useState('')
  const handleAdder = () => {
    if (inputValue.trim() !== '') {
      handleAddTo(inputValue)
    }
  }

  return (
    <form className={className}>
      <Textarea
        required
        size='sm'
        name='Size'
        placeholder='Enter title'
        autoFocus
        onKeyDown={e => {
          if (e.key === 'Enter') {
            e.preventDefault()
            handleAdder()
          }
        }}
        onChange={e => {
            setInputValue(e.target.value)
        }}
        defaultValue=''
      />

      <div className='addbutton'>
        <Button
          type='submit'
          size='sm'
          onClick={e => {
            e.preventDefault()
            handleAdder()
          }}
        >
          Add
        </Button>

        <CloseIcon
          size='sm'
          className='closeIcon'
          onClick={() => {
            handleCancel()
          }}
        />
      </div>
    </form>
  )
}

export default ListForm
