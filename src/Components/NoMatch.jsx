import React from 'react'

const NoMatch = () => {
  return (
    <div>
      <h3
        style={{
          color: 'red',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100vh'
        }}
      >
       Page Not Found
      </h3>
    </div>
  )
}

export default NoMatch
