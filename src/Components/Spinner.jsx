import React from 'react'
import { Box, CircularProgress } from '@mui/material'

function Spinner () {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <CircularProgress disableShrink />
    </Box>
  )
}

export default Spinner
