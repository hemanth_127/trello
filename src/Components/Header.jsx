import React from 'react'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import Avatar from '@mui/material/Avatar'
import Tooltip from '@mui/material/Tooltip'
import { NavLink } from 'react-router-dom'
import Button from '@mui/material/Button'

function Header () {
  return (
    <div className='header'>
      <AppBar
        position='fixed'
        sx={{
          backgroundColor: 'hsl(210,12%,13%)',
          border: '1px solid #31383D',
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center'
        }}
      >
        <Toolbar disableGutters>
          <NavLink to='/' style={{ textDecoration: 'none' }}>
            <Typography
              variant='h6'
              href='#app-bar-with-responsive-menu'
              sx={{
                mr: 2,
                ml: 5,
                fontWeight: 700,
                color: 'inherit',
                textDecoration: 'none'
              }}
            >
              Trello
            </Typography>
          </NavLink>
          <NavLink to='/'>
            <Button color='inherit'>Boards</Button>
          </NavLink>
        </Toolbar>
        <div style={{ flexGrow: 1 }}></div>
        <Tooltip title='Open settings'>
          <IconButton sx={{ p: 0 ,mr:'2rem'}} >
            <Avatar alt='Remy Sharp' src='/static/images/avatar/2.jpg' />
          </IconButton>
        </Tooltip>
      </AppBar>
    </div>
  )
}

export default Header
