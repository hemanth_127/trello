import React from 'react'
import Board from '../TrelloComponent/Boards/Board.jsx'


const Home = () => {
  return (
    <div style={{ marginTop: '4rem' }}>
      <Board />
    </div>
  )
}

export default Home
