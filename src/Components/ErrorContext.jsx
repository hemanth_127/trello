import React, { createContext, useState } from 'react'

export const ErrorContext  = createContext()

const ErrorProvider = ({ children }) => {
  const [error, setError] = useState(null)
  

  return (
    <ErrorContext.Provider
      value={{error, setError }}
    >
      {error ? (
        <div
          style={{
            color: 'red',
            display: 'flex',
            fontWeight: 'bolder',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh'
          }}
        >
          Error: {error}
        </div>
      ) : (
        children
      )}
    </ErrorContext.Provider>
  )
}

export default ErrorProvider
