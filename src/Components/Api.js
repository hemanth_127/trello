import axios from 'axios'

const key = '957ab6245da25bd25fe1260d895a6a48'
const token =
  'ATTA7106db92f39147fe26f35b14beea4fc7f9a94afe51b4ac53401f186fcae102fdBB34AC10'

const url = 'https://api.trello.com/1/'

axios.defaults.params = {
  key: key,
  token: token
}

export const getAllBoards = async () => {
  const response = await axios.get(
    `${url}/members/me/boards?fields=name,url`
  )
  return response
}

export const createBoardByName = async boardName => {
  const response = await axios.post(
    `${url}/boards/?name=${boardName}`
  )
  return response
}

export const getAllLists = async boardId => {
  const response = await axios.get(
    `${url}/boards/${boardId}/lists?`
  )
  return response
}

export const createList = async (boardId, listName) => {
  const response = await axios.post(
    `${url}/boards/${boardId}/lists?name=${listName}`
  )

  return response
}

export const deleteListById = async listId => {
  const response = await axios.put(
    `${url}/lists/${listId}/closed?&value=true`
  )
  return response
}
export default getAllBoards

export const getAllCards = async listId => {
  const response = await axios.get(
    `${url}/lists/${listId}/cards?`
  )
  return response
}

export const deleteCardById = async cardId => {
  const response = await axios.delete(
    `${url}/cards/${cardId}?`
  )
  return response
}

export const createCard = async (listId, cardName) => {
  const response = await axios.post(
    `${url}/cards?idList=${listId}&name=${cardName}`
  )

  return response
}

export const getCheckList = async cardId => {
  const response = await axios.get(
    `${url}/cards/${cardId}/checklists?`
  )
  return response
}
export const createCheckListById = async (cardId, name) => {
  const response = await axios.post(
    `${url}/cards/${cardId}/checklists?&name=${name}`
  )
  return response
}
export const deleteCheckListById = async (cardId, checkListId) => {
  const response = await axios.delete(
    `${url}/cards/${cardId}/checklists/${checkListId}?`
  )
  return response
}

export const getCheckItems = async checkListId => {
  const response = await axios.get(
    `${url}/checklists/${checkListId}/checkItems?`
  )
  return response
}

export const createCheckItems = async (checkListId, name) => {
  const response = await axios.post(
    `${url}/checklists/${checkListId}/checkItems?name=${name}`
  )
  return response
}

export const deleteCheckItemById = async (checkListId, checkItemId) => {
  const response = await axios.delete(
    `${url}/checklists/${checkListId}/checkItems/${checkItemId}?`
  )
  return response
}

export const strikeCheckItems = async (
  cardId,
  checkListId,
  itemId,
  newState
) => {
  const response = await axios.put(
    `${url}/cards/${cardId}/checklist/${checkListId}/checkItem/${itemId}?&state=${newState}`
  )
  return response
}
